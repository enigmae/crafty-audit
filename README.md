# Crafty Audit

A program that will download Lynis, run it, and generate a pretty HTML report for you.

### Credit to Lynis
Please support the [Lynis project](https://cisofy.com/lynis/). This tool basically uses their software, and then parses the report that is produced. Without their project, this wouldn't exists.