#get lynis
wget https://github.com/CISOfy/lynis/archive/master.zip

#unzip, set perms
unzip -o master.zip
chown -R 0:0 lynis-master

#audit the system
cd lynis-master
./lynis audit system  --report-file ../lynis-report.dat

#make report readable
chmod 777 ../lynis-report.dat

#generate the html file
cd ../
python test.py


#clean up
rm -rf lynis-master
rm -f master.zip.*
